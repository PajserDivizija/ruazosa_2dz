package hr.fer.ruazosa.calculator

object Calculator {
    var result: Double = 0.0
    var expression: MutableList<String> = mutableListOf()

    fun reset() {
        result = 0.0
        expression = mutableListOf()
    }

    fun addNumber(number: String) {
        try {
            number.toDouble()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }
        if (expression.count() % 2 == 0) {
            expression.add(number)
        } else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    fun addOperator(operator: String) {
        if (expression.count() % 2 != 1) {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "*" -> expression.add(operator)
            "/" -> expression.add(operator)
            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }

    fun evaluate() {
        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }
        val simplifiedExpression: MutableList<String> = mutableListOf() //expression without multiply
        // or divide ops
        for(i in 1 until expression.count() step 2){
            var value: Number?
            when(expression[i]){
                "*" -> {
                    value = expression[i-1].toDouble() * expression[i+1].toDouble()
                    if(i >= 2){
                        if(expression[i-2] == "-"){
                            value *= -1
                        }
                    }
                    simplifiedExpression.add(value.toString())
                }
                "/" -> {
                    value = expression[i-1].toDouble() / expression[i+1].toDouble()
                    if(i >= 2){
                        if(expression[i-2] == "-"){
                            value *= -1
                        }
                    }
                    simplifiedExpression.add(value.toString())
                }
                else -> {
                    simplifiedExpression.add(expression[i-1])
                    simplifiedExpression.add(expression[i])
                }
            }
        }
        if(simplifiedExpression.count() == expression.count()-1){ /* After the first mathematical expression is
        given and something else is entered the variables simplifiedExpression and expression aren't
        the same size (difference is 1) because the else block above doesn't add the i+1 element of expression to
        simplifiedExpression. This misallingment triggers an ArrayOutOfBoundsException
        */
            simplifiedExpression.add(expression.last())
        }

        result = simplifiedExpression[0].toDouble()
        for (i in 1 until simplifiedExpression.count() step 2) {
            when (simplifiedExpression[i]) {
                "+" -> result += simplifiedExpression[i + 1].toDouble()
                "-" -> result -= simplifiedExpression[i + 1].toDouble()
                "*" -> result *= simplifiedExpression[i + 1].toDouble() //should never get executed
                "/" -> result /= simplifiedExpression[i + 1].toDouble() //should never get executed
            }
        }
    }
}